### Microstimulation in a spiking neural network model of the midbrain superior colliculus

_Accepted to PLoS Computational Biology_

Code is developed under Ubuntu (14.4 LTS) to run on a Tesla K40 GPU card with CUDA Toolkit 8.0.

Note that dynamic parallelism requires compute capability 3.5 and ptx and GPU code generation and separate compilation.

### Installation / Compilation

Clone the repository go to the folder and run 'make'. This will generate an executable 'microstimulation'.
Debug folder is also provided for development with Nsight.

### Run

Run the executable with the desired simulation protocol and parameters. Commented lines give an example of calling the method with parameters to embed simulation into a broader pipeline. (can be used for parameter tuning, sweeping or optimization)

### Simulation Results

The simulation results are saved as .csv files with a name describing simulation parameters under the folder run/.

Simulation create 5 files with the filename summarizing the simulation parameters.

_neuroninfo.csv_ has 5 columns showing:
```bash
id posx posy tauw nospks
```
_spiketimes.csv_ has 2 columns showing:
```bash
spiketimes id
```
_snapshot_ files are showing the excitatory, inhibitory conductances and microstimulation current on each cell of 201x201 matrix.

Resulting plots and plotting functions are given in Microstimulation ipython notebook, they depend on myhelpers.py
