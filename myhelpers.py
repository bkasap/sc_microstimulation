from sys import stdout
import pandas as p
from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.gridspec as gridspec
import numpy as np
import matplotlib.colors as mcolors

def simpleaxis(ax):
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left() 

def getspiketrain(spikes, neuronid):
    train = []
    for i in spikes:
        if i[1]==neuronid:
            train.append(i[0])
    if not train:
        #print "no spikes!"
        return np.array([0]);
    else:
        return np.array(train)

def frate(st, window_width=5, dt=0.01):
    '''
    st : spike train
    window_width : gaussian kernel width in ms
    dt : time step in ms
    '''
    width_dt = int(window_width/dt)
    window = exp(-arange(-4 * width_dt, 4 * width_dt + 1) ** 2 * 1. / (2 * (width_dt) ** 2))
    rate = zeros(int(1000./(dt)))
    if len(st)>0:
        st = [int(x/dt) for x in st]
        rate[st] = 1000./dt
    return convolve(rate, window * 1. / sum(window), mode='same')

def colorpic1(val):
    cmap = plt.get_cmap('Blues')
    return cmap(int(val*256))

def colorpic2(val):
    cmap = plt.get_cmap('PRGn')
    return cmap(int(val*256))

def colorpic3(val):
    cmap = plt.get_cmap('viridis')
    return cmap(int(val*256))

def eyetrace(filenamebase):
    neuron = p.read_csv(filenamebase+"neuroninfo.csv", ' ')
    spikes = p.read_csv(filenamebase+"spiketimes.csv", ' ')
    neuron = neuron.values
    spikes = spikes.values

    ccid = neuron[0,0]
    stcent = getspiketrain(spikes, ccid)
    start = stcent[0]
    
    time =  []#[0]
    hor = []#[0]
    ver = []#[0]
    for spike in spikes:
        time.append(spike[0])
        hor.append(exp(neuron[int(spike[1])][1])*cos(neuron[int(spike[1])][2]))
        ver.append(exp(neuron[int(spike[1])][1])*sin(neuron[int(spike[1])][2]))

    #px = exp(u)*cos(v)
    #px = exp(u)*sin(v)
        
    #print 'eyetrace: \n',
    #print spike[1], 'neuron[spike[1]+1][1]=', neuron[spike[1]+1][1]
    time.append(spike[0])
    hor.append(hor[-1])
    ver.append(ver[-1])
    return time, hor, ver

from scipy import interpolate
def savitzky_golay(y, window_size, order, deriv=0, rate=1):
    r"""Smooth (and optionally differentiate) data with a Savitzky-Golay filter.
    The Savitzky-Golay filter removes high frequency noise from data.
    It has the advantage of preserving the original shape and
    features of the signal better than other types of filtering
    approaches, such as moving averages techniques.
    Parameters
    ----------
    y : array_like, shape (N,)
        the values of the time history of the signal.
    window_size : int
        the length of the window. Must be an odd integer number.
    order : int
        the order of the polynomial used in the filtering.
        Must be less then `window_size` - 1.
    deriv: int
        the order of the derivative to compute (default = 0 means only smoothing)
    Returns
    -------
    ys : ndarray, shape (N)
        the smoothed signal (or it's n-th derivative).
    Notes
    -----
    The Savitzky-Golay is a type of low-pass filter, particularly
    suited for smoothing noisy data. The main idea behind this
    approach is to make for each point a least-square fit with a
    polynomial of high order over a odd-sized window centered at
    the point.
    Examples
    --------
    t = np.linspace(-4, 4, 500)
    y = np.exp( -t**2 ) + np.random.normal(0, 0.05, t.shape)
    ysg = savitzky_golay(y, window_size=31, order=4)
    import matplotlib.pyplot as plt
    plt.plot(t, y, label='Noisy signal')
    plt.plot(t, np.exp(-t**2), 'k', lw=1.5, label='Original signal')
    plt.plot(t, ysg, 'r', label='Filtered signal')
    plt.legend()
    plt.show()
    References
    ----------
    .. [1] A. Savitzky, M. J. E. Golay, Smoothing and Differentiation of
       Data by Simplified Least Squares Procedures. Analytical
       Chemistry, 1964, 36 (8), pp 1627-1639.
    .. [2] Numerical Recipes 3rd Edition: The Art of Scientific Computing
       W.H. Press, S.A. Teukolsky, W.T. Vetterling, B.P. Flannery
       Cambridge University Press ISBN-13: 9780521880688
    """
    import numpy as np
    from math import factorial

    try:
        window_size = np.abs(np.int(window_size))
        order = np.abs(np.int(order))
    except ValueError, msg:
        raise ValueError("window_size and order have to be of type int")
    if window_size % 2 != 1 or window_size < 1:
        raise TypeError("window_size size must be a positive odd number")
    if window_size < order + 2:
        raise TypeError("window_size is too small for the polynomials order")
    order_range = range(order+1)
    half_window = (window_size -1) // 2
    # precompute coefficients
    b = np.mat([[k**i for i in order_range] for k in range(-half_window, half_window+1)])
    m = np.linalg.pinv(b).A[deriv] * rate**deriv * factorial(deriv)
    # pad the signal at the extremes with
    # values taken from the signal itself
    firstvals = y[0] - np.abs( y[1:half_window+1][::-1] - y[0] )
    lastvals = y[-1] + np.abs(y[-half_window-1:-1][::-1] - y[-1])
    y = np.concatenate((firstvals, y, lastvals))
    return np.convolve( m[::-1], y, mode='valid')

def interpolateandsmooth(tim, pos):
    """
    Interpolation and smooting from raw spikes.
    """
    f = interpolate.interp1d(tim, pos, kind='slinear')
    tnew = np.linspace(tim[0], tim[-1], 201)
    pnew = f(tnew)
    ysg = savitzky_golay(pnew, window_size=51, order=4)
    return tnew, ysg

