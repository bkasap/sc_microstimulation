microstimulation: microstimulation.o

microstimulation.o: microstimulation.cu
	nvcc -G -g -O0 -gencode arch=compute_35,code=sm_35  -odir "." -M -o "$(@:%.o=%.d)" "$<"
	nvcc -G -g -O0 --compile --relocatable-device-code=true -gencode arch=compute_35,code=compute_35 -gencode arch=compute_35,code=sm_35  -x cu -o  "$@" "$<"

microstimulation: microstimulation.o
	nvcc --cudart static --relocatable-device-code=true -gencode arch=compute_35,code=compute_35 -gencode arch=compute_35,code=sm_35 -link -o  microstimulation  microstimulation.o

all: microstimulation

clean:
	rm *.o *.d microstimulation

rmdata:
	rm -rf run